# vue-style-beautify

[![](https://vsmarketplacebadge.apphb.com/version-short/xiguaxigua.vue-style-beautify.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=xiguaxigua.vue-style-beautify)
[![](https://vsmarketplacebadge.apphb.com/installs-short/xiguaxigua.vue-style-beautify.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=xiguaxigua.vue-style-beautify)

### Features

- beautify you css rules
- better css order
- auto change after save
- selection beautify

### Quick Start

- Install [vue-style-beautify](https://marketplace.visualstudio.com/items?itemName=xiguaxigua.vue-style-beautify)
- key: cmd+shift+i
- settings: vueStyle.formatOnSave (default `true`)

### LICENSE

MIT
